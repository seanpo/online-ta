/*************************************************************
* Classes
*************************************************************/ 

var Track = function (title, vidID, duration) {
  var _this = this;

  this.title = title || null;
  this.vidID = vidID || null;
  this.duration = duration || null;
  this.currentTime = 0;

  this.setCurrentTime = function ( time ) {
    _this.currentTime = time;
  };

  this.JSONdumps = function () {
    return JSON.stringify(_this);
  };
};

var User = function (socket, name) {
  var _this = this;

  if (!socket) {
    console.error("A socket is not optional.");
    return null;
  }
  this.socket = socket || "";
  this.name = name || "";

  this.setName = function (name) {
    _this.name = name; 
    _this.socket.emit('set_nickname', {nickname: _this.name});
    $(document).trigger("changed_nickname");
  };

  socket.emit('user_added', {nickname: _this.name});
}

var Playlist = function () {
  var _this = this;
  
  /*********************************************************
  * Public variables
  *********************************************************/
  this.name = "";
  this.trackList = [];

  /*********************************************************
  * trackList get and set functions
  *********************************************************/

  this.empty = function () {
    _this.trackList = [];
  };

  this.length = function() {
    return _this.trackList.length;
  };

  this.findTrack = function (identifier) {
    for (index in _this.trackList) {
      if ( _this.trackList[index].vidID === identifier || 
           _this.trackList[index].name === identifier ){
        return index;
      }
    }
    return -1;
  };
  
  this.addTrack = function (name, vidID, duration) {
    var track = new Track(name, vidID, duration);
    _this.pushTrack(track);
  };

  this.pushTrack = function (track) {
    _this.trackList.push(track);
    return track;
  };

  this.popTrack = function () {
    return _this.trackList.slice(0,1);
  };

  this.removeTrack = function (identifier) {
    var index = _this.findTrack(identifier);
    if ( index !== -1 ) {
      _this.trackList.splice(index, 1);
    } 
    return index;
  };

  this.getTrack = function (identifier) {
    var index = _this.findTrack(identifier);
    if ( index !== -1 ) {
       return this.trackList[index];
    } 
    return false;
  };

  /*********************************************************
  * name get and set functions
  *********************************************************/

  this.hasName = function () {
    return _this.name !== "";
  };

  this.setName = function (name) {
    _this.name = name;
  };

  /*********************************************************
  * Utility functions 
  *********************************************************/

  this.JSONloads = function (json) {
    var results = JSON.parse(json); 
    var trackList = [];
    
    // This for loop will actually iterate once only.
    // This is just to extract the name.

    for (index in results) {
      _this.setName(index);
      trackList = results[index];
    }

    _this.trackList = []; // Make sure current track list is empty

    for (index in trackList) {
      track = trackList[index];

      // We pass in a track object so that built in functions can
      // still be used.

      _this.trackList.push(new Track(track.name, track.vidID));
    }
        
  };

  this.JSONdumps = function (json) {
    data = {};
    data[_this.name] = _this.trackList;
    return JSON.stringify(data); 
  };
}

// Options will hold the locations where things will be held,
// it will look like the following:
//
// { announcement: 'dom_ID_of_location_where_announcement_will_be_shown',
//   new_track: 'dom_ID_of_location_where_queued_songs_will_be_shown',
//   search_results: 'dom_ID_of_location_where_search_results_will_be_shown',
//   search_result: 'class_of_individual_search_result',
//   ytPlayer: {
//               width : 'XX',
//               height : 'XX'
//             }
// }

var Room = function (socket, name, id, user, options) {
  options = options || {};

  var _this = this;

  this.playTimeout = 1000;

  this.setPlayTimeout = function (num) {
    _this.playTimeout = num;
  };

  if (!socket) {
    console.error("A socket is not optional.");
    return null;
  }
  this.socket = socket;
 
  this.name = name || "";
  this.id = id || "";

  this.user = user || null;
  this.userList = [];

  this.currentTrack = null;
  this.trackList = new Playlist();

  /*********************************************************
  * trackList server communication functions
  *********************************************************/

  this.addTrack = function (title, vidID, duration) {
    var track = new Track(title, vidID, duration)
    _this.socket.emit('trackAdded', track.JSONdumps());
  };
  
  /***********************************************************
   * Get and set functions for the playlist list
  ***********************************************************/

  this.savePlaylist = function (playlist) {
    _this.socket.emit('playlistCreated', playlist.JSONdumps()); 
  };
  
  this.removePlaylist = function (name) {
    _this.socket.emit('playlistRemoved', name);
  };

  /***********************************************************
   * Room utility functions. 
  ***********************************************************/

  var createOnID = function (name) {
    return name + "_" + _this.id;
  };

  /***********************************************************
   * YouTube functions. 
  ***********************************************************/

  this.ytPlayer;

  this.populateSearchResults = function (data) {
    var entries = data.feed.entry || [];
    var $search_results = $('#' + (options.search_results || "search_results"));
    var search_result = options.search_result || "search_result";

    $search_results.empty();
    if ($search_results) {
      for (var i=0; i<entries.length; i++) {
        var entry = entries[i]; 

        (function (title, vidID, duration, thumbnail) {
          var resultDiv = $('<div class="' + search_result  + '">' +
                               '<img src="' + thumbnail + '"> </img>' + 
                               '<div>' + title + '</div>' + 
                            '</div>');

          resultDiv.click(function() {
            _this.addTrack(title, vidID, duration);
            $search_results.empty();
          }); 

          $search_results.append(resultDiv);
        })(entry.title.$t, entry.media$group.yt$videoid.$t, entry.media$group.yt$duration.seconds, entry.media$group.media$thumbnail[0].url);
      }
    }
  };

  this.initYTPlayer = function () {
    var ytPlayer = options.ytPlayer;
    var width = ytPlayer? ytPlayer.width : undefined;
    var height = ytPlayer? ytPlayer.height : undefined; 

    _this.ytPlayer = new YT.Player('ytapiplayer', {
      height : height || '390',
      width : width || '640',
    });
  };

  this.playCurrentTrack = function (time) {

    // Just to fix a race condition. Theres GOT to be a better way.

    setTimeout( function () {
       _this.ytPlayer.loadVideoById(_this.currentTrack.vidID, time);
    }, _this.playTimeout);
  };

  this.seekTo = function (time){

    // Just to fix a race condition. Theres GOT to be a better way.

    setTimeout( function () {
      _this.ytPlayer.seekTo(time, true);
    }, _this.playTimeout);
  };

  /***********************************************************
   * Initialize the Room. 
  ***********************************************************/

  _this.socket.on(createOnID("announcement"), function (data) {
    $(document).trigger("new_announcement", {message : data.message});
  });

  this.likeCurrent = function () {
    _this.socket.emit('like', _this.currentTrack.JSONdumps());
  };

  this.skip = function () {
    _this.socket.emit('skip');
  };

  _this.socket.on(createOnID('app_status_update'), function (data) {
    if (_this.ytPlayer) {
      if (data.currentTrack != null) {
        if (_this.currentTrack === null || data.currentTrack.vidID != _this.currentTrack.vidID) {
          _this.currentTrack = new Track(data.currentTrack.title, data.currentTrack.vidID, data.currentTrack.duration)
          _this.playCurrentTrack(data.currentTime);
        } else if (_this.ytPlayer.getCurrentTime ){
          if (Math.abs(_this.ytPlayer.getCurrentTime() - data.currentTime) > 5) {
            _this.seekTo(data.currentTime, true);
          }
        } else {
          console.error("Soundsmith: Cannot get youtube Currenttime.");
        }

        _this.currentTrack.setCurrentTime(data.currentTime);
        $(document).trigger('update_song');
      } else {
        $(document).trigger('no_new_song');
      }

      _this.trackList.empty();

      for(var i = 0; i < data.playlist.length; i++ ) {
        var track = data.playlist[i];
        _this.trackList.addTrack( track.title, track.vidID, track.duration );
      }
      $(document).trigger("update_tracklist");

      _this.userList = [];
      for(var socketId in data.connectedSockets) {
        var user = data.connectedSockets[socketId];
        _this.userList.push(user.nickname);
      }
      $(document).trigger("update_userlist");
    }
  });
};

var Messenger = function (socket, roomID) {
  var _this = this;

  if (!socket) {
    console.error("A socket is not optional.");
    return null;
  }
  this.socket = socket;

  this.messages = [];
  this.latestMessage = "";
  this.length = 0;
  
  roomID = roomID? "_" + roomID : "";
  _this.socket.on("new_message" + roomID, function (data) {
    _this.latestMessage = data.message;
    _this.messages.push(data.message);
    _this.length++;
    $(document).trigger('new_message');
  });

  this.sendMessage = function (message){
    _this.socket.emit('send_message', { message : message });
  };
};
