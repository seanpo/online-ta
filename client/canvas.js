// This is a hack that allows you to pass in data upon socket connection
io.MySocket = function(your_info, host, options){
    io.Socket.apply(this, [host, options]);
      this.on('connect', function(){
        this.send({__special:your_info});
      });
};
io.util.inherit(io.MySocket, io.Socket);
////////////////////////////////////////////////////////////////////////////////
var socket = null,
	nickname = null,
	boxy = null
  clientList = [];

$(document).ready(function() {
  // Set up username;
  boxy = new Boxy($('#set_nickname_container'));
	init();
});

////////////////////////////////////////////////////////////////////////////////

function init()
{
  var draw = new HTML5Draw('canvas');
	// setup socket.io
	socket = io.connect();

	socket.on('connect', function() {
	});

	socket.on('disconnect', function() {});

	socket.on('announcement', function(data) {
		$('#activity_container').prepend('<div>' + data.msg + '</div>');
	});

	socket.on('serverUpdate', function(data) {
    var action = new DrawAction();
    action.setHistory(data);
    // update listening users
    draw.actionList.push(action);
    draw.redraw();
	});

  draw.$canvas.bind( "updateCanvas", function (action ) {
     socket.emit( "action", action );
  });

  draw.$canvas.bind( "stanzaComplete", function () {
    socket.emit("stanzaComplete", draw.stanza);
  });
}
