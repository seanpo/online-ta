// This object keeps track of the current user, and user information
DrawClient = function (name) {
  var _this = this;
  this.name = name; 
  this.id = -1;

  // This function expects an integer id; 
  this.setID = function (id) {
    _this.id = id; 
  }

  this.init = function ( id, url ) {
    _this.setID(id);
  }

  this.toJSON = function () {
    return JSON.stringify({"name":_this.name,
                           "id":_this.id
                          });
  }
}

