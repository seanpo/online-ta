NICKNAME_COOKIE = "nickname";

// This is a hack that allows you to pass in data upon socket connection
io.MySocket = function(your_info, host, options){
    io.Socket.apply(this, [host, options]);
      this.on('connect', function(){
        this.send({__special:your_info});
      });
};
io.util.inherit(io.MySocket, io.Socket);
////////////////////////////////////////////////////////////////////////////////
var socket = null,
	nickname = null,
	boxy = null
  clientList = [];

$(document).ready(function() {
  // Set up username;
  // boxy = new Boxy($('#set_nickname_container'));
	init();
});

////////////////////////////////////////////////////////////////////////////////

function init()
{

  var draw = new HTML5Draw('canvas');
	// setup socket.io
	socket = io.connect();

  messenger = new Messenger(socket);

  // Set up user. Check to see if the user has a nickname 
  // stored in the cookie.

  var cachedName = readCookie(NICKNAME_COOKIE); 
  var nickname = cachedName? cachedName : "Guest";
  var user = new User(socket, nickname);

  // set the default message, and make the enter button send the message
  var messageID = "message_input_field";
  setDefault(messageID);
  onEnter(messageID, function () {
    $('#send').trigger('click'); 
  });

  $('#send').click( function () {
    var $message_input_field = $('#' + messageID);
    var val = $message_input_field.val();
    messenger.sendMessage("<br/> <div class='text'>" + user.name + " said: " + val +"</div><br/>");
    $message_input_field.val("");
  });

	socket.on('connect', function() {
	});

	socket.on('disconnect', function() {});

	socket.on('announcement', function(data) {
		$('#activity_container').prepend('<div>' + data.msg + '</div>');
	});

	socket.on('serverUpdate', function(data) {
    var action = new DrawAction();
    action.setHistory(data);
    // update listening users
    draw.actionList.push(action);
    draw.redraw();
	});

  $(document).bind("new_message", function ( ){
    $('#comment_container').prepend(messenger.latestMessage);
  });

  draw.$canvas.bind( "stanzaComplete", function () {
    var length = draw.actionList.length;
    // The reason why the latest stanza is popped is because it will be pushed back in upon serverUpdate event
    var stanza = draw.actionList.pop().toJSON();
    // We send the server the action list in JSON, and the index inside actionList in which it resides
    socket.emit("updateCanvas", stanza);
  });
}

/*function setNickname()
{
  nickname = $('#nickname_box').val();
  boxy.hide();
  if (!nickname){
    Boxy.alert('You must have a name.');
    Boxy.show();
  }
}
*/
