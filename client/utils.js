/*********************************************************** 
* Utility functions
***********************************************************/

function createCookie(name,value,days) {
  if (days) {
    var date = new Date();
    date.setTime(date.getTime()+(days*24*60*60*1000));
    var expires = "; expires="+date.toGMTString();
  }
  else 
    var expires = "";
  document.cookie = name+"="+value+expires+"; path=/";
}

function readCookie(name) {
  var nameEQ = name + "=";
  var ca = document.cookie.split(';');
  for(var i=0;i < ca.length;i++) {
    var c = ca[i];
    while (c.charAt(0)==' ') c = c.substring(1,c.length);
    if (c.indexOf(nameEQ) == 0) return c.substring(nameEQ.length,c.length);
  }
  return null;
}

function eraseCookie(name) {
  createCookie(name,"",-1);
}

function getBaseURL() {
  var location = window.location;
  return location.protocol + "//" + location.hostname + ":" + location.port;
}

// Call back is needed because this function is asynchronous.
function getContent(path, cb) {
  $.ajax({
    type: 'GET',
    traditional: true,
    url: getBaseURL() + "/" + path,
    success: function (data){
      cb(data);
    }
  });
}

// Sets text input areas to take on the default class, and have a default value,
// Which will disappear upon focus. and will reappear upon blur if the value
// is nothing.

function setDefault( id ) {
  var $element = $('#' + id);

  $element.val($element.attr('default')).addClass('default');
  $element.focus( function () { 
    if ($element.val() == $element.attr('default')) {
      $element.removeClass('default');
      $element.val('');
    }   
  }); 

  $element.blur( function () { 
    if (!$element.val()) {
      $element.addClass('default');
      $element.val($element.attr('default'));
    }   
  }); 
}

// This function sets the dom element to run the call back if
// enter is pressed.

function onEnter( id, cb ) {
  var $element = $('#' + id);
  $element.keyup( function (e)  {
    if (e.keyCode === 13) {
      cb();
    }
  });
}
  
