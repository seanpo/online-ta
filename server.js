/*SETUP_VARIABLES*/
var NODE_PORT = 3000,
    NODE_HOST = "localhost";
///////////////////////////////////////////////////////////////////////////////

var http = require('http'),
	url = require('url'),
	path = require('path'),
	fs = require('fs'),
	mime = require('mime'),
	io = require('socket.io'),
	sanitizer = require('sanitizer');

////////////////////////////////////////////////////////////////////////////////

var UserList = function() {
  var _this = this;
	this.userList = [];
  this.length = 0;

  this.createUser = function(name, id){
    _this.userList.push({
      "name" : name,
      "id": id //socket.id 
    });
    _this.length++
  };

  this.removeUser = function (id){
    for ( i = 0 ; i < _this.length ; i ++ ){
      if ( _this.userList[i]['id'] === id ){
        _this.userList.splice(i,1);
      }
    }
  };

};


var History = function() {
  var _this = this;
  this.history = {
    // This is to detect a paintbucket click.
    'paintBucket' : [],  
    // This is to detect where the users have clicked.
    'click' : {
              'x': [],
              'y': []
            }, 
    // Boolean to check if the users are dragging
    'drag' : [], 
    // A number that determines the size of each stroke in pixels.
    'size' : [],
    // color will be rgba 
    'color' : []
  };

  // This is a flag to make sure that no two clients are updating at the same time.
  this.updating = false;
  
  this.setHistory = function( dataJSON ) {
    var stanza = JSON.parse( dataJSON );
     _this.history = stanza;
  }

  this.update = function( dataJSON ){
    _this.updating = true;

    var stanza = JSON.parse( dataJSON ),
        _history = _this.history,
        stanzaX = stanza['click']['x'],
        stanzaY = stanza['click']['y'];
        
    for ( index in stanzaX ){
      _history['click']['x'].stanza( stanzaX[index] );
      _history['click']['y'].stanza( stanzaY[index] );
    }

    _history['drag'].concat( stanza['drag'] );
    _history['size'].concat( stanza['size'] );
    _history['color'].concat( stanza['color'] );

    _this.updating = false;

  };

};

userList = new UserList();
mainHistory = [];

////////////////////////////////////////////////////////////////////////////////

// etart the node server
var server = http.createServer( function ( request, response ) {
	var requestPath = url.parse( request.url ).pathname;
  console.log( requestPath );
	requestPath = ( requestPath == '/' ) ? '/index.html' : requestPath;
	requestPath = './client' + requestPath;

	path.exists( requestPath, function( exists ) {
		if ( !exists ) {
			response.writeHead(404, {'Content-Type': 'text/plain'});  
			response.end('404 Not Found\n', 'binary');  
		} else {
			fs.readFile(requestPath, 'binary', function(error, data) {  
				if(error) {  
					response.writeHead(500, {'Content-Type': 'text/plain'});  
					response.end(error + '\n', 'binary');  
				} else {
					response.writeHead(200, {'Content-Type': mime.lookup(requestPath)});  
					response.end(data, 'binary');
				}
			});  
		}
	});
});

server.listen(NODE_PORT, NODE_HOST);

////////////////////////////////////////////////////////////////////////////////

// start socket.io
io = io.listen(server);

io.sockets.on('connection', function( socket ) {
  userList.createUser("sean", socket.id);
  update();

  socket.on('send_message', function(data) {
    io.sockets.emit('new_message', {message : data.message});
  });
	
  socket.on('updateCanvas' , function ( canvasJSON ){
    var history = new History();
    history.setHistory(canvasJSON);

    mainHistory.push(history);
    console.log(history);
    update();
  });

	socket.on('disconnect', function() {
	});

});

////////////////////////////////////////////////////////////////////////////////

// start everything up
//setInterval(update, appConfig.updateFrequency * 1000);

////////////////////////////////////////////////////////////////////////////////

function update()
{
  var index = mainHistory.length - 1;
  if (index >=0 ){
    console.log(index);
    io.sockets.emit('serverUpdate', mainHistory[index].history, index);
  }
}

